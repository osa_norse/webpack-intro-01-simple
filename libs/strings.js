export function capitalize(str) {
    var firstLetter = str.substr(0, 1).toUpperCase();
    var restOfStr = str.substr(1);
    return firstLetter + restOfStr;
}